apiVersion: apps/v1
kind: Deployment
metadata:
  name: authing-server-orbiter
  namespace: authing
spec:
  strategy:
    type: RollingUpdate
    rollingUpdate:
      maxUnavailable: 0
      maxSurge: 1
  replicas: 1
  selector:
    matchLabels:
      app.kubernetes.io/name: authing-server-orbiter
      app.kubernetes.io/part-of: authing-server-orbiter
  template:
    metadata:
      labels:
        app.kubernetes.io/name: authing-server-orbiter
        app.kubernetes.io/part-of: authing-server-orbiter
    spec:
      affinity:
        podAntiAffinity:
          preferredDuringSchedulingIgnoredDuringExecution:
            - podAffinityTerm:
                labelSelector:
                  matchExpressions:
                    - key: app
                      operator: In
                      values:
                        - authing-server-orbiter
                topologyKey: failure-domain.beta.kubernetes.io/zone
              weight: 90
      imagePullSecrets:
        - name: regsecret
      terminationGracePeriodSeconds: 120
      initContainers:
        - name: init-migration
          image: registry.cn-beijing.aliyuncs.com/authing-next/authing-server-orbiter:${ORBITER_VERSION}
          command:
            [
              "yarn",
              "migration:run",
            ]
          volumeMounts:
            - name: authing-server-orbiter-config
              mountPath: /etc/authing-server-orbiter
              readOnly: true
          securityContext:
            privileged: true

      volumes:
        - name: authing-server-orbiter-config
          configMap:
            name: authing-server-orbiter-config
        - name: authing-server-orbiter-logs
          hostPath:
            path: /mnt/logs/authing-server
        - configMap:
            defaultMode: 420
            name: authing-server-logstash-config
          name: authing-server-logstash-config
      containers:
        - name: authing-server-orbiter
          ports:
            - containerPort: 3000
              protocol: TCP
          image: registry.cn-beijing.aliyuncs.com/authing-next/authing-server-orbiter:${ORBITER_VERSION}
          imagePullPolicy: Always
          volumeMounts:
            - name: authing-server-orbiter-config
              mountPath: /etc/authing-server-orbiter
              readOnly: true
            - name: authing-server-orbiter-logs
              mountPath: /var/log/authing-server
          resources:
            limits:
              cpu: 500m
              memory: 1024Mi
            requests:
              cpu: 250m
              memory: 256Mi
        - name: authing-server-orbiter-logstash
          image: logstash:6.8.22
          volumeMounts:
            - name: authing-server-orbiter-logs
              mountPath: /var/log/authing-server
            - name: authing-server-logstash-config
              mountPath: /usr/share/logstash/pipeline/logstash-access.conf
              subPath: logstash-access.conf
            - name: authing-server-logstash-config
              mountPath: /usr/share/logstash/pipeline/logstash-audit.conf
              subPath: logstash-audit.conf
            - name: authing-server-logstash-config
              mountPath: /usr/share/logstash/pipeline/logstash-error.conf
              subPath: logstash-error.conf
            - name: authing-server-logstash-config
              mountPath: /usr/share/logstash/pipeline/logstash-user-action.conf
              subPath: logstash-user-action.conf
            - name: authing-server-logstash-config
              mountPath: /usr/share/logstash/pipeline/logstash-metrics.conf
              subPath: logstash-metrics.conf
            - name: authing-server-logstash-config
              mountPath: /usr/share/logstash/pipeline/logstash-webhook.conf
              subPath: logstash-webhook.conf
            - name: authing-server-logstash-config
              mountPath: /usr/share/logstash/pipeline/logstash-pipeline.conf
              subPath: logstash-pipeline.conf
            - name: authing-server-logstash-config
              mountPath: /usr/share/logstash/pipeline/logstash-sms-sent.conf
              subPath: logstash-sms-sent.conf
            - name: authing-server-logstash-config
              mountPath: /usr/share/logstash/pipeline/logstash-custom-database-script-execution.conf
              subPath: logstash-custom-database-script-execution.conf
            - name: authing-server-logstash-config
              mountPath: /usr/share/logstash/config/logstash.yml
              subPath: logstash.yml
            - name: authing-server-logstash-config
              mountPath: /usr/share/logstash/pipeline/logstash.conf
              subPath: logstash.conf
            - name: authing-server-logstash-config
              mountPath: /usr/share/logstash/config/pipelines.yml
              subPath: pipelines.yml
            - name: authing-server-logstash-config
              mountPath: /usr/share/logstash/pipeline/logstash-provisioning.conf
              subPath: logstash-provisioning.conf
            - name: authing-server-logstash-config
              mountPath: /usr/share/logstash/pipeline/logstash-synchronization-node.conf
              subPath: logstash-synchronization-node.conf

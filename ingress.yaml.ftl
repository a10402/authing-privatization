apiVersion: networking.k8s.io/v1
kind: Ingress
metadata:
  annotations:
    kubernetes.io/ingress.class: "nginx"
    nginx.ingress.kubernetes.io/auth-tls-verify-client: "off"
    nginx.ingress.kubernetes.io/auth-tls-verify-depth: "1"
    nginx.ingress.kubernetes.io/ssl-redirect: "true"
    nginx.ingress.kubernetes.io/proxy-buffer-size: "64k"
    nginx.ingress.kubernetes.io/proxy_buffers: "32 32k"
    nginx.ingress.kubernetes.io/proxy_busy_buffers_size: "128k"
    nginx.ingress.kubernetes.io/proxy-connect-timeout: "120"
    nginx.ingress.kubernetes.io/proxy-send-timeout: "120"
    nginx.ingress.kubernetes.io/proxy-read-timeout: "120"
    nginx.ingress.kubernetes.io/auth-tls-pass-certificate-to-upstream: "true"
  name: authing-server
  namespace: authing
spec:
  tls:
  - hosts:
    - "*.uls.authing-inc.co"
    secretName: uls-authing-inc-co-cert
  - hosts:
    - "console.uls.authing-inc.co"
    secretName: uls-authing-inc-co-cert
  - hosts:
    - "static.uls.authing-inc.co"
    secretName: uls-authing-inc-co-cert
  rules:
    - host: "*.${PRIMARY_DOMAIN}"
      http:
        paths:
          - path: /
            pathType: Prefix
            backend:
              service:
                name: authing-server
                port: 
                  number: 5000
    - host: "static.${PRIMARY_DOMAIN}"
      http:
        paths:
          - path: /
            pathType: Prefix
            backend:
              service:
                name: authing-staticfiles
                port: 
                  number: 80
    - host: "docs.${PRIMARY_DOMAIN}"
      http:
        paths:
          - path: /
            pathType: Prefix
            backend:
              service:
                name: authing-docs
                port:
                  number: 80
    - host: "kuboard.${PRIMARY_DOMAIN}"
      http:
        paths:
          - path: /
            pathType: Prefix
            backend:
              service:
                name: kuboard-v3
                port:
                  number: 80

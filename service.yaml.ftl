apiVersion: v1
kind: Service
metadata:
  name: authing-docs
  namespace: authing
  labels:
    run: authing-docs
spec:
  type: ClusterIP
  selector:
    run: authing-docs
  ports:
    - port: 80
      name: docs-port
      targetPort: 80
---
kind: Service
apiVersion: v1
metadata:
  name: authing-server
  namespace: authing
  labels:
    app.kubernetes.io/name: authing-server
    app.kubernetes.io/part-of: authing-server
spec:
  type: ClusterIP
  selector:
    app.kubernetes.io/name: authing-server
    app.kubernetes.io/part-of: authing-server
  ports:
    - name: http
      port: 5000
      targetPort: 3000
    - name: ldap
      port: 1389
      targetPort: 1389
---
apiVersion: v1
kind: Service
metadata:
  name: authing-staticfiles
  namespace: authing
  labels:
    run: authing-staticfiles
spec:
  type: ClusterIP
  selector:
    run: authing-staticfiles
  ports:
    - port: 80
      name: statics-port
      targetPort: 80

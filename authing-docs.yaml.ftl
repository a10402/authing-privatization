apiVersion: apps/v1
kind: Deployment
metadata:
  name: authing-docs
  namespace: authing
spec:
  selector:
    matchLabels:
      run: authing-docs
  replicas: 1
  template:
    metadata:
      labels:
        run: authing-docs
    spec:
      imagePullSecrets:
        - name: regsecret
      containers:
        - name: authing-docs
          image: registry.cn-beijing.aliyuncs.com/authing-public/authing-server-public:${DOCS_VERSION}
          ports:
            - containerPort: 80
              protocol: TCP

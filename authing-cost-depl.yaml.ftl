apiVersion: apps/v1
kind: Deployment
metadata:
  name: authing-cost-privatization
  namespace: authing
spec:
  strategy:
    type: RollingUpdate 
    rollingUpdate:
      maxUnavailable: 0
      maxSurge: 1
  replicas: 1
  selector:
    matchLabels:
      app.kubernetes.io/name: authing-cost-privatization
      app.kubernetes.io/part-of: authing-cost-privatization
  template:
    metadata:
      labels:
        app.kubernetes.io/name: authing-cost-privatization
        app.kubernetes.io/part-of: authing-cost-privatization
    spec:
      affinity:
        podAntiAffinity: 
          preferredDuringSchedulingIgnoredDuringExecution:
            - podAffinityTerm:
                labelSelector:
                  matchExpressions:
                    - key: app
                      operator: In
                      values:
                        - authing-cost-privatization
                topologyKey: kubernetes.io/hostname
              weight: 100
      imagePullSecrets:
        - name: regsecret
      containers:
        - name: authing-cost-privatization
          image: registry.cn-beijing.aliyuncs.com/authing-next/authing-cost-privatization:${COST_VERSION}
          imagePullPolicy: IfNotPresent
          ports:
            - containerPort: 8888
              protocol: TCP
          resources:
            limits:
              cpu: 100m
              memory: 100Mi
            requests:
              cpu: 10m
              memory: 10Mi
          livenessProbe:
            failureThreshold: 3
            httpGet:
              path: /actuator/health
              port: 8888
              scheme: HTTP
            initialDelaySeconds: 3
            periodSeconds: 30
            successThreshold: 1
            timeoutSeconds: 1
          readinessProbe:
            failureThreshold: 3
            httpGet:
              path: /actuator/health
              port: 8888
              scheme: HTTP
            initialDelaySeconds: 3
            periodSeconds: 30
            successThreshold: 1
            timeoutSeconds: 1
          volumeMounts:
          - name: authing-cost-privatization-conf-map
            mountPath: /opt/conf/config.yaml
            subPath: config.yaml
      volumes:
      - name: authing-cost-privatization-conf-map
        configMap:
          name: authing-cost-privatization-conf
          items:
          - key: config.yaml
            path: config.yaml

---
apiVersion: v1
kind: Service
metadata:
  name: authing-cost-privatization
  namespace: authing
spec:
  selector:
    app.kubernetes.io/name: authing-cost-privatization
  ports:
    - port: 8888
      targetPort: 8888
      name: authing-cost-privatization


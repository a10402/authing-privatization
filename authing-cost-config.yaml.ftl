apiVersion: v1
kind: ConfigMap
metadata:
  name: authing-cost-privatization-conf
  namespace: authing
data:
  config.yaml: |
    level: info
    server:
      port: 8888
      run_mode: release
    db:
      driver: PostgreSQL
      url: host=${DATABASE_URL} user=${DATABASE_USERNAME} password=${DATABASE_PASSWORD} dbname=${DATABASE_NAME} port=${DATABASE_PORT} sslmode=disable TimeZone=Asia/Shanghai
    redis:
      host: ${REDIS_URL}:${REDIS_PORT}
      pwd: ${REDIS_PASSWORD}
      db: 1

apiVersion: v1
kind: ConfigMap
metadata:
  name: authing-server-orbiter-config
  namespace: authing
data:
  config.yaml: |-
    database:
      type: postgres
      synchronize: false
      url: postgres://${DATABASE_USERNAME}:${DATABASE_PASSWORD}@${DATABASE_URL}:${DATABASE_PORT}/${DATABASE_NAME}

    logging:
      path:
        provisioning: /var/log/authing-server/provisioning/provisioning.log
        default: /var/log/authing-server/sync-center/default.log
        synchronization: /var/log/authing-server/synchronization/synchronization.log
        synchronizationNode: /var/log/authing-server/synchronization-node/synchronization-node.log

    redis:
      url: redis://:${REDIS_PASSWORD}@${REDIS_URL}:${REDIS_PORT}
      
    search:
      type: es
      url: http://${ES_URL}:${ES_PORT}
      username: ${ES_USERNAME}
      password: ${ES_PASSWORD}
      
    analysis:
      type: es
      url: http://${ES_URL}:${ES_PORT}
      username: ${ES_USERNAME}
      password: ${ES_PASSWORD}

      patterns:
        provisioning: authing-server-log-provisioning-*

    queue:
      enabled: true
      redis:
        host: ${REDIS_URL}
        port: ${REDIS_PORT}
        password: ${REDIS_PASSWORD}
        db: 1

    proxy:
      httpProxy: socks5://authing:7face500bc18@47.90.98.12:6080
      matchDomains:
        - googleapis.com
        - github.com
        - amazonaws.com.cn
        - githubusercontent
      
    authing-server:
      url: http://authing-server.authing.svc.cluster.local:5000

    worker:
      scannerCronExpression: '* * * * *'
      maxWorker: 1
      fetchJobSize: 5
      enabled: true

    websockets:
      enabled: true
      host: ws://core.${PRIMARY_DOMAIN}:8855
      port: 8855
      path: /connections/adconnector
    secret:
      privateKey: |
        -----BEGIN RSA PRIVATE KEY-----
        MIIEogIBAAKCAQEAtl+/DW3Kfyqbe6g/cDipU4vYuen5JfGoR7+r2UR4h08Zb9x5
        zjU6iTeMuFn8dCMO7v4mxcW2bsb0ro7ex/xipMxoSH2xfy7jZ4WXkxoXPT1ySKwl
        VXMcHhfcXHksfDx9KgMQnUldZcQmIVob7zyeXsqE76msd2ryFJct3chyGy5yqg8N
        jP+8pqXCUUxVA1LF5tVSC3O5Zf6H1cD6P67R3jNyqKzj0owiRYBUNE0NW+RDH+nJ
        HaLhs4rIGRxSkT38NMvUAY1Ek/HXt7L8eFup7xWI407EFVAQuXc4Lp1chMF7hvTy
        VXF/JV1ccqpGrpnc8ZkdgfE6QvN8R2QdbkEwaQIDAQABAoIBABOI9gIOPJ6ahcUd
        JI50w057sF9ZvKJf3FKzKUlAmFiGFIdy6mk4Mmj4Vk549R3Lk5HuD2En6H69OQBp
        tVteoYJdkt4Q+K5S63DvUmkjZlHc/xzhs64PgneGvsf5Atbb89xIL8NpVGPOOvdf
        m5Zou94Te2HgnuS7w+qItxUGn5JhnP1PpS0nnPLWaS+CwHT7IiNlhYEW9jDq+Fc7
        TwG5rycVmDZ+VaKWFHZbdMGReVrB1Q41pInH6vZ9KJ+2kX5vgdGz4wuW6otnStuf
        FSFymkXJjZVq99pBIKR+HrYczgnPcBQ03TIwo35iIi9rpGzQ+CQheRlLvD7OgRSs
        aT5iG7ECgYEA36/a89f7P/wTv8GJzzzn5htAp01CDxHOnACTItIb497m1GSYeV9t
        raK17DcXVDSKd64B7Eh8bSXw2sLILqXmYI2gdaCuMoo1OFpgE0awGMAOqur0gG4W
        rpVIXdEV+XR0nTrEi5C5SKSFGplbB0K8uKor8QI0PAqIygZWxqa3AdsCgYEA0Lga
        C4dU7hAeVchb+lWHpHrtd+Kk7ZQuaqiOUzupXImzoJi8mRgbbVOWN6tLyeuS+28u
        gkTeg8gXHXEysTrN5gCGXRWtZa6lyNS4Qy0DGUPkdwYwpTHo52gOpR1dOa/5EhgU
        V+ye6oqPAqGFXumNUQVJ+X1fugHC83zN0OptFAsCgYAHnx1Nu6ky5+rgWL7SUdfw
        7jLIwIgkrU2l4RsiT5hlrj2dNQRA9nBtPyMFtmbUFqo1I7erDlk2+tj3CzX46Owt
        lAXInD3zOORRVDr7zSfI4Bpf1nuYOKgLMMjzcWFXS5GBnfP+cWLux7RFUL5aQ/rc
        K+gtYQ7YkN9SSt8AC8QmiwKBgADUyt3C4zazf7ttudgMYSbpimqQz4FWtrycAT0Z
        nroMnleEZDPj4bAUxiNgnkOmmYZA0hmYtp4lvNnUKpSEHG/PDrnuLSubf3OGTVmP
        I1UFbTKAJs9xnHDXfD9yF330UnSc/e+/tH5QoPMsu4A+Q97Pu5SvyCLQgPtBQvyT
        Am4zAoGASz+zMmgv8T44GdVnfikRqr3DHx/giuKgJArEobCE8llu7ZakG/PRH31l
        bXo2cp2saYrah+p8mcQemNiEKiQ2TJ634oROpdVcyjzD/G6qCwQiU4J/XE6kprB4
        DEPt1b/gQa4oPdFjbbEQHixdSzoAYPqxvnSOzroNkDOAVCglkNE=
        -----END RSA PRIVATE KEY-----
      publicKey: |
        -----BEGIN PUBLIC KEY-----
        MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAtl+/DW3Kfyqbe6g/cDip
        U4vYuen5JfGoR7+r2UR4h08Zb9x5zjU6iTeMuFn8dCMO7v4mxcW2bsb0ro7ex/xi
        pMxoSH2xfy7jZ4WXkxoXPT1ySKwlVXMcHhfcXHksfDx9KgMQnUldZcQmIVob7zye
        XsqE76msd2ryFJct3chyGy5yqg8NjP+8pqXCUUxVA1LF5tVSC3O5Zf6H1cD6P67R
        3jNyqKzj0owiRYBUNE0NW+RDH+nJHaLhs4rIGRxSkT38NMvUAY1Ek/HXt7L8eFup
        7xWI407EFVAQuXc4Lp1chMF7hvTyVXF/JV1ccqpGrpnc8ZkdgfE6QvN8R2QdbkEw
        aQIDAQAB
        -----END PUBLIC KEY-----


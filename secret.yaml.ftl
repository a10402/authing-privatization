apiVersion: v1
kind: Namespace
metadata:
  name: authing
  labels:
    name: authing
---
apiVersion: v1
kind: Secret
metadata:
  name: regsecret
  namespace: authing
data:
  .dockerconfigjson: eyJhdXRocyI6eyJyZWdpc3RyeS5jbi1iZWlqaW5nLmFsaXl1bmNzLmNvbSI6eyJ1c2VybmFtZSI6IndhbmdjaGVua2FpQGF1dGhpbmciLCJwYXNzd29yZCI6ImF1dGhpbmcucmVwby4xMjMiLCJhdXRoIjoiZDJGdVoyTm9aVzVyWVdsQVlYVjBhR2x1WnpwaGRYUm9hVzVuTG5KbGNHOHVNVEl6In19fQ==
type: kubernetes.io/dockerconfigjson

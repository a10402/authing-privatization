kind: Service
apiVersion: v1
metadata:
  name: authing-server-orbiter
  namespace: authing
  labels:
    app.kubernetes.io/name: authing-server-orbiter
    app.kubernetes.io/part-of: authing-server-orbiter
spec:
  type: ClusterIP
  selector:
    app.kubernetes.io/name: authing-server-orbiter
    app.kubernetes.io/part-of: authing-server-orbiter
  ports:
    - name: authing-server-orbiter
      port: 3000
      targetPort: 3000
    - name: socket
      port: 8855
      targetPort: 8855

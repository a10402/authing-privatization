apiVersion: apps/v1
kind: Deployment
metadata:
  name: authing-staticfiles
  namespace: authing
spec:
  selector:
    matchLabels:
      run: authing-staticfiles
  replicas: 1
  template:
    metadata:
      labels:
        run: authing-staticfiles
    spec:
      imagePullSecrets:
        - name: regsecret
      containers:
        - name: authing-staticfiles
          image: registry.cn-beijing.aliyuncs.com/authing-public/authing-server-public:${STATIC_VERSION}
          ports:
            - containerPort: 80
              protocol: TCP
